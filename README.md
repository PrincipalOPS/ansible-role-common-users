# ansible-role-common-users

## Add to galaxy-roles.yml

```bash
- src: git+ssh://git@bitbucket.org/PrincipalOps/ansible-role-common-users.git
  name: common-users
```

## Install Role

```bash
# Use --force to UPDATE existing role to LATEST version
ansible-galaxy install -r galaxy-roles.yml --force
```
